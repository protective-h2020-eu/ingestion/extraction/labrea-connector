#!/bin/bash
#
### BEGIN INIT INFO
# Provides:          labrea-idea
# Required-Start:    $local_fs $syslog
# Required-Stop:     $local_fs $syslog
# Should-Start:      $network
# Should-Stop:       $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Labrea-Idea aggregator/converter
### END INIT INFO

DAEMON_NAME=labrea-idea
DAEMON_PATH=/usr/local/bin/"$DAEMON_NAME".py
PID=/var/run/"$DAEMON_NAME".pid

# Try Debian & Fedora/RHEL/Suse sysconfig
for n in default sysconfig; do
	[ -f /etc/$n/"$DAEMON_NAME" ] && . /etc/$n/"$DAEMON_NAME"
done

# Fallback
function log_daemon_msg () { echo -n "$@"; }
function log_end_msg () { [ $1 -eq 0 ] && echo " OK" || echo " Failed"; }
function status_of_proc () { [ -f "$PID" ] && ps u -p $(<"$PID") || echo "$PID not found."; }

[ -f /lib/lsb/init-functions ] && . /lib/lsb/init-functions

ACTION="$1"

case "$ACTION" in
	start)
		if [ -z "$PARAMS" ]; then
			log_daemon_msg "Unconfigured $DAEMON_NAME, not starting."
			exit 2
		fi
		mkdir -p "${PID%/*}"
		log_daemon_msg "Starting $DAEMON_NAME"
		start_daemon -p "$PID" "$DAEMON_PATH" --pid "$PID" $PARAMS
		log_end_msg $?
		;;
	stop)
		log_daemon_msg "Stopping $DAEMON_NAME"
		killproc -p "$PID" "$DAEMON_PATH"
		log_end_msg $?
		;;
	restart|force-reload)
		$0 stop && sleep 2 && exec $0 start
		;;
	status)
		status_of_proc -p "$PID" "$DAEMON_PATH"
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 2
		;;
esac
